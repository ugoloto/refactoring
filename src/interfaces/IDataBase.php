<?php


namespace App\Interfaces;


interface IDataBase
{
    public function connection();
    public function read(): string;
    public function write(string $content): int;
}