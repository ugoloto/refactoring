<?php


namespace App\Interfaces;


interface IMailer
{
    public function sendEmail();
    public function setTo($to);
    public function setFrom($from);
    public function setMessage($message);
}