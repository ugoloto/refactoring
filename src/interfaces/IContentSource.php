<?php


namespace App\Interfaces;


interface IContentSource
{
    public function getContent(string $address): string;
}