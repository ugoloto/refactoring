<?php


namespace App\Classes;



use App\Interfaces\IContentSource;

class ContentSourceFile implements IContentSource
{

    public function getContent(string $address): string
    {
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Accept-language: en"
            )
        );

        $context = stream_context_create($opts);

        return file_get_contents($address, false, $context);
    }
}