<?php


namespace App\Classes;


use App\Interfaces\IMailer;

class Mail implements IMailer
{
    private $to;
    private $from;
    private $message;

    public function sendEmail(){

        printf("Email has been send to %s From %s.\r\n\r\n Notify you about %s", $this->to, $this->from, $this->message);
    }
    public function createEmail(){

        printf("Email has been send to %s From %s.\r\n\r\n Notify you about %s", $this->to, $this->from, $this->message);
    }

    /**
     * @param mixed $to
     * @return Mail
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @param mixed $from
     * @return Mail
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @param mixed $message
     * @return Mail
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

}