<?php
namespace App\Classes;

use App\Interfaces\IDataBase;

class DBFile implements IDataBase
{
    public $db;
    public $user;
    public $pass;

    public function __construct()
    {
        $this->connection();
    }

    public function read():string
    {
        return file_get_contents($this->db);
    }

    public function write(string $content):int
    {
        return file_put_contents($this->db, $content, FILE_APPEND);
    }

    final public function connection()
    {
        $this->db = DB_DATABASE;
        $this->user = DB_USERNAME;
    }
}