<?php

namespace App\Classes;

use App\Interfaces\IContentSource;
use App\Interfaces\IDataBase;
use App\Interfaces\IMailer;

class App
{
    private $database;
    private IMailer $mailer;
    private $contentSource;
    public function __construct(IDataBase $dataBase, IMailer $mailer, IContentSource $contentSource)
    {
        $this->database = $dataBase;
        $this->mailer = $mailer;
        $this->contentSource = $contentSource;

    }

    public function doIt($value, $adress='http://artii.herokuapp.com/make?text=', $text='REFACTORING'){

        if ($value >= 3 && $value < 6) {
            $message = 'Your Value is too low';
        } elseif ($value == 7) {
            $message = 'Your Value equals to 7';
        }
        $content = $this->contentSource->getContent($adress . $text);

        $this->database->write($content);

        $this->mailer->setMessage($message);
        $this->mailer->;
        $this->mailer->sendEmail();

    }

}