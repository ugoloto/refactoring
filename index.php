<?php
use App\Classes\App;
use App\Classes\ContentSourceFile;
use App\Classes\DBFile;
use App\Classes\Mail;
ini_set('display_errors', 1);

if (is_file('config.php')) {
    require_once('config.php');
}

require "vendor/autoload.php";


$to = 'test@example.com';
$from = 'admin@example.com';

$mailer = new Mail();
$mailer
    ->setTo($to)
    ->setFrom($from)
    ->createEmail();

$db = new DBFile();

$source = new ContentSourceFile();

$app=new App($db, $mailer, $source);

$app->doIt(3);
